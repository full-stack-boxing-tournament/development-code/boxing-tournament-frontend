import { extendTheme } from '@chakra-ui/react';

const customTheme = extendTheme({
  styles: {
    global: {
      body: {
        color: 'white',
      },
    },
  },
  colors: {
    primary: {
      firstPrimary: 'null right now',
      secondPrimary: 'null right now'
    },
    secondary: {
      firstSecondary: '#222831',
      secondSecondary: ''
    },
  },
  fonts: {
    heading: 'Montserrat, sans-serif',
    body: 'Roboto, sans-serif',
    // ...
  },
});

export default customTheme;
