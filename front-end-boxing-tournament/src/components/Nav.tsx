import {Box, Button} from "@chakra-ui/react"
import {Input} from '@chakra-ui/react'
import customTheme from '../theme/chakra-theme.ts'
import { useState } from "react"
import { SearchIcon } from '@chakra-ui/icons'

interface Props {
    onSearch: (input: string) => void;
}

function Nav({onSearch}: Props) {
    const [search, setSearch] = useState<string>("")

    function updateSearch(input: string): void{
        setSearch(input)
    }

    function onClickSearch() {
        onSearch(search)
        setSearch("")
    }

    const secondaryColor: string = customTheme.colors.secondary.firstSecondary

    return (
        <Box
            paddingTop={6}
            height="12vh"
            backgroundColor={secondaryColor}>
            <Box paddingLeft="25%">
                <Box display="inline" padding={3}>
                    <Button
                    onClick={onClickSearch}
                    colorScheme='teal'
                    variant='outline'>
                        <SearchIcon />
                    </Button>    
                </Box>
                <Box display="inline">
                    <Input
                        margin="auto"
                        variant="flushed"
                        placeholder='search fighter'
                        size='md'
                        width="30vw"
                        height="2rem"
                        border="none"
                        colorScheme="teal"
                        value={search}
                        onChange={e => updateSearch(e.target.value)}
                    />
                </Box>
            </Box>
        </Box>
           
    )
}

export default Nav