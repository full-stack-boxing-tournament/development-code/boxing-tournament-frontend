import { Flex } from "@chakra-ui/react"
import LeftConference from "./LeftConference"
import RightConference from "./RightConference"
import CenterPanel from "./CenterPanel"

function Body() {
    return (
        <Flex flexDirection="row" bg="green">
            <LeftConference />
            <CenterPanel />
            <RightConference />
        </Flex>
    )  
}

export default Body