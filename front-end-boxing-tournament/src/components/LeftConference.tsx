import { Box } from "@chakra-ui/react"

function LeftConference() {
 
    return (
        <Box flex='1' height='100vh' flexGrow={3} bg="purple">
            Left
        </Box>
    )
  }

  export default LeftConference
