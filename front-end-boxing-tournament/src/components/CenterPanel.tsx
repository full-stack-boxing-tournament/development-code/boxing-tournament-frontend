import { Box } from "@chakra-ui/layout"

function CenterPanel() {
    return (
        <Box flex='2' height='100vh' flexGrow={10}>
          Center
        </Box>
    )
}

export default CenterPanel