import { Flex } from "@chakra-ui/react";
import Nav from "./components/Nav.tsx";
import Body from "./components/Body";
import { useState } from "react";

function App() {
  const [query, setQuery] = useState<string>("")

  function updateQuery(input: string): void{
    setQuery(input)
  }

  console.log(query)

  return (
    <Flex flexDirection="column">
        <Nav 
        onSearch={(data) => updateQuery(data)}
        />
        <Body />
    </Flex>
  )
}

export default App
